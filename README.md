### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

Run this project by this command :

`mvn clean spring-boot:run`

### Screenshot

Home Product Page

![Home Page](img/home.png "Home Page")

List Products Page

![List Products Page](img/list.png "List Product Page")

New Product Page

![New Product Page](img/new.png "New Product Page")

View Product Page

![View Product Page](img/view.png "View Product Page")

Edit Product Page

![Edit Product Page](img/edit.png "Edit Product Page")