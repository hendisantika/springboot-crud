package com.hendisantika.crud.service;

import com.hendisantika.crud.entity.Product;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crud
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/09/18
 * Time: 07.10
 * To change this template use File | Settings | File Templates.
 */
public interface ProductService {
    Iterable<Product> listAllProducts();

    Optional<Product> getProductById(Integer id);

    Product saveProduct(Product product);

    void deleteProduct(Integer id);
}
