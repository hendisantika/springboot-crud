package com.hendisantika.crud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crud
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/09/18
 * Time: 07.12
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class IndexController {

    @GetMapping("/")
    String index() {
        return "index";
    }

    @GetMapping("/hi")
    String home() {
        return "Spring is here!";
    }

}